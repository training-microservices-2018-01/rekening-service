# Aplikasi Rekening Service #

Fitur

* Create Rekening
* List Rekening
* List Mutasi

## Daftar Endpoint ##

* Buat rekening baru : `POST /{nasabah}/`

    ```json
    {
        "nama" : "Endy - DEPOSITO",
        "nomor" : "D-001",
        "jenisRekening" : "DEPOSITO"
    }
    ```

* Lihat daftar rekening : `GET /{nasabah}/`

    ```json
    [
        {
            "id": "30f65894-9065-413a-b6a2-967e51ef3a19",
            "nasabah": "abc123",
            "nomor": "G-001",
            "nama": "Endy - Giro",
            "jenisRekening": "GIRO"
        },
        {
            "id": "380b50ba-1190-4d5a-9012-121bf248bf6e",
            "nasabah": "abc123",
            "nomor": "D-001",
            "nama": "Endy - DEPOSITO",
            "jenisRekening": "DEPOSITO"
        },
        {
            "id": "a653d2b2-061c-4b35-a3ad-b61bac31a38d",
            "nasabah": "abc123",
            "nomor": "T-001",
            "nama": "Endy - TABUNGAN",
            "jenisRekening": "TABUNGAN"
        }
    ]
    ```

* Lihat mutasi untuk rekening `rek123` : `GET http://localhost:8080/rek123/mutasi`

    ```json
    [
        {
            "id": "m001",
            "rekening": {
                "id": "30f65894-9065-413a-b6a2-967e51ef3a19",
                "nasabah": "abc123",
                "nomor": "G-001",
                "nama": "Endy - Giro",
                "jenisRekening": "GIRO"
            },
            "keterangan": "Setoran Tunai",
            "waktuTransaksi": "2018-08-01T08:59:59",
            "nilai": 10000000
        },
        {
            "id": "m002",
            "rekening": {
                "id": "30f65894-9065-413a-b6a2-967e51ef3a19",
                "nasabah": "abc123",
                "nomor": "G-001",
                "nama": "Endy - Giro",
                "jenisRekening": "GIRO"
            },
            "keterangan": "ATM 001",
            "waktuTransaksi": "2018-08-02T08:59:59",
            "nilai": 100000
        },
        {
            "id": "m003",
            "rekening": {
                "id": "30f65894-9065-413a-b6a2-967e51ef3a19",
                "nasabah": "abc123",
                "nomor": "G-001",
                "nama": "Endy - Giro",
                "jenisRekening": "GIRO"
            },
            "keterangan": "Pembayaran Listrik a.n. Endy",
            "waktuTransaksi": "2018-08-03T08:59:59",
            "nilai": 120000
        },
        {
            "id": "m004",
            "rekening": {
                "id": "30f65894-9065-413a-b6a2-967e51ef3a19",
                "nasabah": "abc123",
                "nomor": "G-001",
                "nama": "Endy - Giro",
                "jenisRekening": "GIRO"
            },
            "keterangan": "Purchase Minimarket EDC 3345",
            "waktuTransaksi": "2018-08-04T08:59:59",
            "nilai": 275000
        },
        {
            "id": "m005",
            "rekening": {
                "id": "30f65894-9065-413a-b6a2-967e51ef3a19",
                "nasabah": "abc123",
                "nomor": "G-001",
                "nama": "Endy - Giro",
                "jenisRekening": "GIRO"
            },
            "keterangan": "Transfer On Us Ref#47475",
            "waktuTransaksi": "2018-08-05T08:59:59",
            "nilai": 250000
        }
    ]
    ```

## Daftar Perintah Kafka ##

* Membuat topic dengan nama `coba`

    ```
    ~/kafka/bin/kafka-topics.sh --zookeeper localhost:2181 --create --replication-factor 1 --partitions 1 --topic coba
    ```

* Melihat daftar topic

    ```
    ~/kafka/bin/kafka-topics.sh --zookeeper localhost:2181 --list
    ```

* Menghapus topic

    ```
    ~/kafka/bin/kafka-topics.sh --zookeeper localhost:2181 --delete --topic coba
    ```

* Listen ke topic `rekening-request`

    ```
    ~/kafka/bin/kafka-console-consumer.sh --bootstrap-server training-microservices-201801.artivisi.id:9092 --topic rekening-request --from-beginning
    ```

* Kirim message ke topic `rekening-request`

    ```
    ~/kafka/bin/kafka-console-producer.sh --broker-list training-microservices-201801.artivisi.id:9092 --topic rekening-request
    ```
