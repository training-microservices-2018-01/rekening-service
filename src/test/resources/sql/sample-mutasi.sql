insert into mutasi (id, id_rekening, keterangan, waktu_transaksi, nilai) values 
('m001', '30f65894-9065-413a-b6a2-967e51ef3a19', 'Setoran Tunai', '2018-08-01 08:59:59', 10000000);

insert into mutasi (id, id_rekening, keterangan, waktu_transaksi, nilai) values 
('m002', '30f65894-9065-413a-b6a2-967e51ef3a19', 'ATM 001', '2018-08-02 08:59:59', 100000);

insert into mutasi (id, id_rekening, keterangan, waktu_transaksi, nilai) values 
('m003', '30f65894-9065-413a-b6a2-967e51ef3a19', 'Pembayaran Listrik a.n. Endy', '2018-08-03 08:59:59', 120000);

insert into mutasi (id, id_rekening, keterangan, waktu_transaksi, nilai) values 
('m004', '30f65894-9065-413a-b6a2-967e51ef3a19', 'Purchase Minimarket EDC 3345', '2018-08-04 08:59:59', 275000);

insert into mutasi (id, id_rekening, keterangan, waktu_transaksi, nilai) values 
('m005', '30f65894-9065-413a-b6a2-967e51ef3a19', 'Transfer On Us Ref#47475', '2018-08-05 08:59:59', 250000);
