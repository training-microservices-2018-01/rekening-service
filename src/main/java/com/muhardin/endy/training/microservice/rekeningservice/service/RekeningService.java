package com.muhardin.endy.training.microservice.rekeningservice.service;

import com.muhardin.endy.training.microservice.rekeningservice.dao.MutasiDao;
import com.muhardin.endy.training.microservice.rekeningservice.dao.RekeningDao;
import com.muhardin.endy.training.microservice.rekeningservice.entity.Mutasi;
import com.muhardin.endy.training.microservice.rekeningservice.entity.Rekening;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service @Transactional 
public class RekeningService {
    
    @Autowired private RekeningDao rekeningDao;
    @Autowired private MutasiDao mutasiDao;
    
    public Rekening create(Rekening rek){
        rekeningDao.save(rek);
        return rek;
    }
    
    public Iterable<Rekening> cariByNasabah(String nasabah){
        return rekeningDao.findByNasabah(nasabah);
    }
    
    public Mutasi create(Mutasi mut){
        mutasiDao.save(mut);
        return mut;
    }
    
    public Iterable<Mutasi> cariByRekening(Rekening rekening){
        return mutasiDao.findByRekeningOrderByWaktuTransaksi(rekening);
    }
}
