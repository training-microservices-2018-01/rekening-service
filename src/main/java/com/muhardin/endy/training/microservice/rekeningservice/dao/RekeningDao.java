package com.muhardin.endy.training.microservice.rekeningservice.dao;

import com.muhardin.endy.training.microservice.rekeningservice.entity.Rekening;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RekeningDao extends PagingAndSortingRepository<Rekening, String> {
    Iterable<Rekening> findByNasabah(String nasabah);
}
