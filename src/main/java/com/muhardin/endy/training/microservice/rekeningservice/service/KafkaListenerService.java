package com.muhardin.endy.training.microservice.rekeningservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.muhardin.endy.training.microservice.rekeningservice.entity.Mutasi;
import com.muhardin.endy.training.microservice.rekeningservice.entity.Rekening;
import java.io.IOException;
import java.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaListenerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaListenerService.class);
    
    @Autowired private ObjectMapper objectMapper;
    @Autowired private RekeningService rekeningService;
    
    @Value("${kafka.topic.rekening.request}")
    private String topicRekeningRequest;
    
    @Value("${kafka.topic.mutasi.request}")
    private String topicMutasiRequest;
    
    @KafkaListener(topics = "${kafka.topic.rekening.request}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleCreateRekening(String message){
        try {
            LOGGER.debug("Topic {} : {}", topicRekeningRequest, message);
            Rekening rek = objectMapper.readValue(message, Rekening.class);
            rekeningService.create(rek);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }
    
    @KafkaListener(topics = "${kafka.topic.mutasi.request}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleCreateMutasi(String message){
        try {
            LOGGER.debug("Topic {} : {}", topicMutasiRequest, message);
            Mutasi m = objectMapper.readValue(message, Mutasi.class);
            m.setWaktuTransaksi(LocalDateTime.now());
            rekeningService.create(m);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }
}
