package com.muhardin.endy.training.microservice.rekeningservice.controller;

import com.muhardin.endy.training.microservice.rekeningservice.entity.Mutasi;
import com.muhardin.endy.training.microservice.rekeningservice.entity.Rekening;
import com.muhardin.endy.training.microservice.rekeningservice.service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MutasiController {
    
    @Autowired private RekeningService rekeningService;
    
    @GetMapping("/{rekening}/mutasi/")
    public Iterable<Mutasi> daftarMutasi(@PathVariable("rekening") Rekening rekening){
        return rekeningService.cariByRekening(rekening);
    }
}
