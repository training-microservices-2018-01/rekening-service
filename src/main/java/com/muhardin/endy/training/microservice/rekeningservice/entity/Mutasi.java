package com.muhardin.endy.training.microservice.rekeningservice.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity @Data
public class Mutasi {
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_rekening")
    private Rekening rekening;
    
    private String keterangan;
    
    @NotNull
    private LocalDateTime waktuTransaksi;
    
    @NotNull
    private BigDecimal nilai;
}
