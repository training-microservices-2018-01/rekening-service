package com.muhardin.endy.training.microservice.rekeningservice.dao;

import com.muhardin.endy.training.microservice.rekeningservice.entity.Mutasi;
import com.muhardin.endy.training.microservice.rekeningservice.entity.Rekening;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MutasiDao extends PagingAndSortingRepository<Mutasi, String> {
    Iterable<Mutasi> findByRekeningOrderByWaktuTransaksi(Rekening r);
}
