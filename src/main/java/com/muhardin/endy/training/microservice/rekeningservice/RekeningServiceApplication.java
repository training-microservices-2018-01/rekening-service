package com.muhardin.endy.training.microservice.rekeningservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RekeningServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RekeningServiceApplication.class, args);
	}
}
