package com.muhardin.endy.training.microservice.rekeningservice.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity @Data
public class Rekening {
    
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    
    @NotNull @NotEmpty
    private String nasabah;
    
    @NotNull @NotEmpty
    private String nomor;
    
    @NotNull @NotEmpty
    private String nama;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private JenisRekening jenisRekening;
}
