package com.muhardin.endy.training.microservice.rekeningservice.controller;

import com.muhardin.endy.training.microservice.rekeningservice.entity.Rekening;
import com.muhardin.endy.training.microservice.rekeningservice.service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RekeningController {
    
    @Autowired private RekeningService rekeningService;
    
    @PostMapping("/{nasabah}/")
    @ResponseStatus(HttpStatus.CREATED)
    public Rekening create(@PathVariable("nasabah") String nasabah, @RequestBody Rekening rek){
        rek.setNasabah(nasabah);
        System.out.println("Rekening : "+rek);
        rekeningService.create(rek);
        return rek;
    }
    
    @GetMapping("/{nasabah}/")
    public Iterable<Rekening> cariRekening(@PathVariable("nasabah")String nasabah){
        return rekeningService.cariByNasabah(nasabah);
    }
}
