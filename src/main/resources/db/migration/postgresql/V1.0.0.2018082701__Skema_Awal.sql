create table rekening (
    id varchar(36),
    nasabah varchar(36) not null,
    nomor varchar(50) not null,
    nama varchar(255) not null,
    jenis_rekening varchar(255) not null,
    primary key (id),
    unique (nomor)
);

create table mutasi (
    id varchar(36),
    id_rekening varchar(36) not null,
    waktu_transaksi timestamp not null,
    nilai numeric(19,2) not null,
    keterangan varchar(255),
    primary key (id),
    foreign key (id_rekening) references rekening(id)
);